# -*- coding: utf-8 -*-
import scrapy


class ImdbSpiderSpider(scrapy.Spider):
    name = 'imdb_spider'
    allowed_domains = ['imdb.com']
    start_urls = ['https://www.imdb.com/chart/top']

    def parse(self, response):
        titles = response.xpath('//td[@class="titleColumn"]')
        for _title in titles:
            title = _title.xpath('.//a/text()').extract_first()
            rating = _title.xpath('.//following-sibling::td[contains(@class,"ratingColumn")]/strong/text()').extract_first()

            yield{
            'title':title,
            'rating': rating
            }
